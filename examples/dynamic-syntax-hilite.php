<?php
/*
 * Example PHP file to show dynamic syntax hilighting
 * This shows a simple page display 10 random winners from an account file
 * MaterializeCSS is used as the responsive framework for the UI
 * I encapsulated all examples in this 1 file.  Of course, I could have used blade, templates, vue.js, ...
 * @author: Ken Colvin
 */

// Put your sql code in heredoc prefixed with sql
$sql = <<<sql
-- SQL syntax highlighting 
select id, acct_level, balance
from  accounts
where balance > 0
group by acct_level
order by acct_level, balance desc
sql;



// Put your HTML code in heredoc prefixed with html
$html = <<<html
<!-- HTML syntax highlighting -->
<div id=foo>
    <a href='mysite.com'>My Link</a>
</div>
html;

// Put your javascript code in heredoc prefixed with javascript
$javascript = <<<javascript
// Javascript syntax highlighting
$(function() {
    $('#foo').on('hover', function() {
        $(this).css('background', 'yellow');
    });
});
javascript;

$max_winners = 10;

$sql_winners = <<<sql_winners
-- Just prefix your heredoc with the language
select  id, email, first_name, last_name, balance, img
from    accounts
where   balance > 0
order by rand();
limit {$max_winners}
sql_winners;

#$rows = MYSQL::rows($sql_winners); // I would call my MYSQL wrapper here, for simplicity hard code rows
$rows = array();
for ($i=1; $i<=$max_winners; $i++) {
    $rows[] = array(
        'id'    => $i,
        'name'  => "Name {$i}",
        'email' => "email{$i}@gmail.com",
        'phone' => sprintf('516-555-00%02d', $i),
        'img'   => "img_{$i}.png",
    );
}

$chunk_size=3; // 3 per chunk/column

ob_start();
?>
    <div class='container center'>
        <h1>Top <?=count($rows)?> Winners</h1>
<? foreach (array_chunk($rows, $chunk_size) as $row) { ?>
        <div class=row>
<? foreach ($row as $acct) { 
    ob_start();
    // This contact_block is repeated, so use this template
?>
                        <p>
                            <a href="mailto:<?=$acct['email']?>">
                                <i class="material-icons">email</i>
                                Email <?=$acct['name']?>
                            </a>
                        </p>
                        <p>
                            <a href="tel:<?=$acct['phone']?>">
                                <i class="material-icons">phone</i>
                                Call <?=$acct['phone']?>
                            </a>
                        </p>
<?
    $contact_block=ob_get_clean();
?>

            <div class='col s12 m12 l<?=12/$chunk_size?>'>
                <div class="card hoverable">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src='https://i.pravatar.cc/600?u=<?=rand(1,123456)?>'>
                        <span class="card-title"><?=$acct['name']?></span>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4"><?=$acct['name']?><i class="material-icons right">more_vert</i></span>
                        <?=$contact_block?>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">Winner Reveal<i class="material-icons right">close</i></span>
                        <p>Another winner, Contact <?=$acct['name']?> and let them know they won gift# <?=$acct['id']?></p>
                        <?=$contact_block?>
                    </div>
                </div>
            </div>
    <? } ?>
        </div>
<? } ?>

    </div>

<?
$contents = ob_get_clean();
?>

<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            /* CSS Syntax hilite */
            #contents {
                display: none;
            }
            .hilite {
                /* my VIM will show the CSS colors by name and/or hex */
                color:            blue;
                background-color: yellow;
                background:       #ffffff;
            }

            .material-icons {
                vertical-align: middle !important;
            }
        </style>
    </head>

    <body>
        <div id='preloader' class="center">
            <h3>Loading Winners...</h3>
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
        <div id='contents' style='display:none'>
        <?=$contents?>
        </div>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

        <script>

            $(function() {

                $(window).on('load', function() {
                    // When the page has loaded
                    $("#preloader").hide();
                    $("#contents").fadeIn(2000);
                });


            });

        </script>

    </body>
</html>
