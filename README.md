
** VIM PHP IDE for your BASH login **

This .vimrc and .vim contains plugins to create an awesome IDE for PHP development

---

Check for PHP errors on write
When you :w your code, this VIM environment will check for any errors
This will save you a lot of debugging time

---

** Dynanic Syntax Highlighting **



![Dynamic Syntax VIM Image](examples/dynamic-syntax-hilite.png)


in PHP use heredocs

```php
$sql = <<<sql
-- SQL syntax highlighting 
select id, acct_level, balance
from  accounts
where balance > 0
group by acct_level
order by acct_level, balance desc
sql;
```

```php
$html = <<<html
<!-- HTML syntax highlighting -->
<div id=foo>
    <a href='mysite.com'>My Link</a>
</div>
html;
```

```php
$javascript = <<<javascript
// Javascript syntax highlighting
$(function() {
    $('#foo').on('hover', function() {
        $(this).css('background', 'yellow');
    });
});
javascript;
```

---

Show me the PHP function manual
shift-k - brings up the PHP manual for the function your cursor is on
Just put cursor on the function, press shift-k, and BAM - you will see the function manual
Here is an example putting the cursor on array_chunk, press shift k, and BAM - the manual for array_chunk

![Shift-K VIM Show PHP manual](examples/alt-k-php-manual.png)

---

EasyAlign - align all code on = or : or any string.  Just do this
put cursor on 1st line
press v to go into visual mark mode
put cursor on last line - press j (or down arrow)
press :
you will then see
:'<,'>

now call the plugin EasyAlign and put string to align by, in this case =

:'<,'>EasyAlign=

You and anyone looking at your code will love you for this

---

TAB - Autocompletes any in memory function names, variables, ...

---

Indentlines 
You will see veritcal indent markers to show you visual branches of the code


