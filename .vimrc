set nocompatible              " be iMproved, required
filetype off                  " required
silent! call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-easy-align'
call plug#end()
execute pathogen#infect()




" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" " alternatively, pass a path where Vundle should install plugins
 "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
Plugin 'beautify-web/js-beautify'
Plugin 'gmarik/Vundle.vim'
Plugin 'itchyny/lightline.vim'

"Plugin 'junegunn/vim-easy-align'
"
" " The following are examples of different formats supported.
" " Keep Plugin commands between vundle#begin/end.

" ,g to activate - Show Functions, Variables, ...
Plugin 'majutsushi/tagbar'

 " html5 completion
Plugin 'othree/html5.vim'
Plugin 'scrooloose/syntastic'

 " This plugin highlights the matching HTML tag when the cursor is positioned
 " on a tag. It works in much the same way as the MatchParen plugin.
Plugin 'gregsexton/MatchTag'
Plugin 'morhetz/gruvbox'

 " This plugin is used for displaying thin vertical lines at each indentation level for code indented with spaces. For code indented with tabs I think there is no need to support it, because you can use :set list lcs=tab:\|\ (here is a space).
 Plugin 'Yggdroot/indentLine'

" Can not turn off HTML rainbow
Plugin 'luochen1990/rainbow'
Plugin 'ap/vim-css-color' " in css show atual colors

"Plugin 'bling/vim-airline'

Plugin 'alvan/vim-phpmanual'	" Shift k to see manual
Plugin 'shawncplus/phpcomplete.vim'
Plugin 'xsbeats/vim-blade'		" Laravel blade
Plugin 'ervandew/supertab'

Plugin 'Shougo/neocomplete.vim'

"
" " All of your Plugins must be added before the following line
" -------------------------------------------------------------

call vundle#end()            " required



set t_Co=256
filetype plugin indent on    " required


"GENERAL SETTINGS
"==================
"let g:airline_theme='molokai'
set laststatus=2	" Always show status line
let mapleader = ","

" Theme Settings
" ==============
syntax enable

function! MyVi(myscheme, mybackground)
	execute 'set background='.a:mybackground
	execute 'colorscheme '.a:myscheme
	if a:myscheme == 'gruvbox'
		if a:mybackground == 'light'
			let g:gruvbox_contrast='hard'
			" Set background color
			" hi normal ctermbg=223 
			" hi CursorLine ctermfg=none ctermbg=230
			" hi Comment ctermfg=none ctermbg=white
		else
			let g:gruvbox_contrast_dark='soft'
			"hi Comment ctermfg=gray ctermbg=red
			" hi normal ctermbg=223 
			"hi normal ctermbg=8
		endif
	endif
	
	if a:myscheme == 'solarized'
		if a:mybackground == 'light'
			" Set background color
			" hi normal ctermbg=223 
			" hi CursorLine ctermfg=none ctermbg=230
			" hi Comment ctermfg=none ctermbg=white
		else
			echo 'dark'
		endif
	endif
endfunction

function! MyGruvBox()
set background=dark
let g:gruvbox_italic=1
colorscheme gruvbox
let g:gruvbox_contrast_dark='soft'
endfunction
call MyGruvBox()

function! MySolarized()
set background=dark
colorscheme solarized
endfunction
"call MySolarized()

"colorscheme jellybeans

"let g:solarized_termcolors=256 " old colors
"colorscheme solarized
"let g:molokai_original = 1 " original background
"let g:rehash256 = 1
"colorscheme mango

"call MyVi('elflord','dark')
"call MyVi('gruvbox','dark')
"call MyVi('molokai','light')
"call MyVi('solarized','dark')
"call MyVi('Chasing_Logic','dark') " does not syntax highlight

" Transparent background so I know which server I am on
"hi Normal ctermbg=none

" PHP Check Erros
" Does NOT check current buffer
map <leader>1 :!php -l %<CR>
" Checks on :w does NOT check on :x
" If not using Syntastic then check php files here:
" au BufWritePost *.php !php -l %
"au BufWinLeave *.php !php -l %
" How to check the current buffer and on :x window close

"au BufWritePre *.php make %
"compiler php
" End PHP Check Erros

"FLAGS
"==================
set nogdefault " gd want control over search/replace
" spell is annoying - do not enable
"set spelllang=en_gb " spelling GB
set nospell
"set nowrap " we like to wrap
set noswapfile " doesn't create a swap file
set cursorline "adds a line for the cursor
set winheight=5
set winminheight=5
set winheight=999
set nonumber
set ignorecase
set incsearch
set smartcase
set wildmenu " shows suggestions when tabing in normal mode
set scrolloff=5 " adds 5 lines to the top and bottom of the window
set clipboard=unnamedplus " sets the system clipboard as default
set nospell		" Annoying


" Formatting {{{
"set noexpandtab   " Make sure that every file uses real tabs, not spaces
set shiftround    " Round indent to pultiple of 'shiftwidth'
set backspace=indent,eol,start " Backspace over everything in insert mode
set smartindent   " Do smart indenting when starting a new line
set autoindent    " Copy indent from current line, over to the new line
set fo=vt         " Set the format options ('formatoptions')
set nojoinspaces  " :h joinspaces

" Set the tab width
let s:tabwidth=4
exec 'set tabstop='    .s:tabwidth
exec 'set shiftwidth=' .s:tabwidth
exec 'set softtabstop='.s:tabwidth
" }}}
"
au VimEnter * if &diff | execute 'windo set wrap' | endif

"KEY BINDINGS
"===================
"This is a bind to navigate windows
nnoremap <F12> :split /var/www/revupmyrestaurant.com/dev/dublindeck/public/assets/triumph/client/css
"nnoremap <F1>  :<ESC>:set paste<CR><S-INS>
nnoremap <INS> :<ESC>:set paste<CR><S-INS>
nnoremap <F8> :$

:nmap <F1> <nop>

nnoremap <Right> <C-w>l
nnoremap <Left> <C-w>h
nnoremap <Up> <C-w>k
nnoremap <Down> <C-w>j

" Toggle commenting Requires T-comment plugin
map <leader>g :Tagbar<CR>
map <silent><leader>w :w<CR>
" This forces syntastic to check for errors
"map <silent><leader>x :w<CR>:q<CR>
"map <silent>:x :w<CR>:q
map <silent><leader>q :q<CR>
" force quite
map <silent><leader>f :q!<CR>

" ABBREVIATIONS
" auto complete tags
iabbrev </ </<C-X><C-O>

" Warn if the file has been changed
au FileChangedShell * echo "Warning: File changed on disk"

"https://github.com/Yggdroot/indentLine
let g:indentLine_enabled = 1
let g:indentLine_char = '│'

let g:javascript_enable_domhtmlcss=1
let g:css_enable_domhtmlcss=1
let g:html5_event_handler_attributes_complete = 1

let g:rainbow_active = 0

"--- Beautify
"map <c-f> :call JsBeautify()<cr>
" or
"autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
"autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
"autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>

"--- Syntastic

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_error_symbol = "✗"
let g:syntastic_warning_symbol = "⚠"
let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd']
let g:syntastic_aggregate_errors = 1
let g:acp_enableAtStartup = 1
let g:syntastic_mode_map={ 'mode': 'active',
                     \ 'active_filetypes': [],
                     \ 'passive_filetypes': ['html'] }

"---NeoComplete - Start config
set nocompatible noshowmode
set hidden hlsearch incsearch
set autoindent smartindent
set expandtab shiftwidth=4 softtabstop=4

"set expandtab shiftwidth=4 softtabstop=4
"set shortmess+=c
"set runtimepath+=$HOME/neocomplete.vim
"syntax enable
"Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 0
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
" Autoselect in popup menu
" This causes window to always popup unnamedplus
let g:neocomplete#enable_auto_select = 0
let g:neocomplete#disable_auto_complete=1


" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
	" Select popup and do NOT create new line
	return pumvisible() ? neocomplete#close_popup() : "\<CR>"

	" For select popup item AND creates a new line
	"return neocomplete#close_popup() . "\<CR>"
endfunction

" NOTE: Do NOT use Space will select and autocomplete, makes it very
" annoying to type
" inoremap <expr><Space> pumvisible() ? "\<C-n>" : "\<Space>"

" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()


" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
let g:neocomplete#sources#omni#input_patterns.php = '\h\w*\|[^. \t]->\%(\h\w*\)\?\|\h\w*::\%(\h\w*\)\?'
inoremap <expr><C-Space> neocomplete#start_manual_complete('omni')
" disable the preivew vim window - too annoying, just have dropdown
set completeopt-=preview
"---NeoComplete - End config

" ---Laravel and PHP stuff
" Abbreviations
abbrev bh :call HtmlBeautify()<cr>
abbrev bj :call JsBeautify()<cr>
abbrev bc :call CSSBeautify()<cr>

"abbrev td :%!tidy --show-errors 0 --show-body-only auto -qi -w 0 --drop-empty-paras no --drop-empty-elements no --indent-spaces 4<cr>

" Edit todo list for project
nmap ,todo :e todo.txt<cr>

" Open splits
nmap vs :vsplit<cr>
nmap sp :split<cr>

" Create/edit file in the current directory
nmap :ed :edit %:p:h/

" Remove search results
command! H let @/=""

"Resize vsplit
"nmap <C-v> :vertical resize +5<cr>
"nmap 25 :vertical resize 40<cr>
"nmap 50 <c-w>=
"nmap 75 :vertical resize 120<cr>

"---Folding - Start
set nofoldenable    " disable folding

function! PHP_FuncName()
	let funcPrefix=""
	let funcName=""
	let fileName=expand('%')
	let fileExt=expand('%:e')

	if 	fileName == ''
		return
	endif

	if 	isdirectory(fileName) != 0
		return 'IsDir'
	endif

	if fileExt != 'php' 
		" just want PHP
		return
	endif

	let fname=split(expand('%:t'))
    let pos = getpos(".")
    let curline = pos[1]
    let win = winsaveview()
    let decl = ""
    "let startline = search('^\s*\(public\)\=\s*\(function\|class\)\s*\w\+','cbW')
    let startline = search('^\s*\(static\|public\|private\)\=\s*\(static\|public\|private\)\=\s*\(static\|public\|private\)\=\s*\(static\|public\|private\)\=\s*\(function\|class\)\s*&\=\w\+', 'cbW')
    call search('{','cW')
    sil exe "normal %"
    let endline = line(".")
    if curline >= startline && curline <= endline
        let decl = getline(startline)
    endif
    call cursor(pos)
    call winrestview(win)
	let isNext=0
	for word in split(decl)
		if isNext == 1
			let funcName=word
			break
		endif
		if tolower(word) == 'function'
			let isNext=1
		endif
	endfor

	if funcName == ''
		let funcName='(noFunc)'
	endif

    return funcName
	"who cares if not php
    "return funcPrefix . funcName
endfunction


function! MyStatusLine()
set ruler
set rulerformat=%l,%c%V%=%P

set statusline=
set statusline+=%f	" Current File
set statusline+=:    " Separator
set statusline+=%{PHP_FuncName()}
set statusline+=%=
set statusline+=col:%-3c
set statusline+=%5l		" Current line
set statusline+=/    " Separator
set statusline+=%L   " Total lines
"-- syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
"-- syntastic
set ruler
endfunction

call MyStatusLine()

    let g:rainbow_conf = {
    \   'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
    \   'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
    \   'operators': '_,_',
    \   'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
    \   'separately': {
    \       '*': {},
    \       'html': {
    \       },
    \		'php': {
    \			'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold', 'start=/(/ end=/)/ containedin=@htmlPreproc contains=@phpClTop', 'start=/\[/ end=/\]/ containedin=@htmlPreproc contains=@phpClTop', 'start=/{/ end=/}/ containedin=@htmlPreproc contains=@phpClTop'],
    \		},
    \       'tex': {
    \           'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
    \       },
    \       'lisp': {
    \           'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick', 'darkorchid3'],
    \       },
    \       'vim': {
    \           'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
    \       },
    \       'css': 0,
    \   }
    \}


"autocmd VimEnter * AnsiEsc
